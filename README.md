# git-templates
Template for Cloudrexx-related GIT repositories

## What's this
This contains all GIT hooks we use to develop for Cloudrexx. Currently this includes:
- PHP Linter
- Commit message linter

This list will probably get longer over time.

## Prerequisites
Obviously, this needs GIT to be installed. In addition here's a list of
requirements by the linters:
- PHP Linter: Either PHP or Docker is required, otherwise this linter is disabled
- Commit message linter: Requires Docker

## Installation
Note: Before you install the templates you need to have `docker` and `git` installed on your computer.

Once `git` and `docker` are installed, go ahead and clone the repository:
```bash
git clone git@bitbucket.org:cloudrexx/git-templates.git ~/.git_template
```

After cloning the repository, do set up your _git templateDir_:
```bash
git config --global init.templateDir '~/.git_template'
```

Finally, add an alias to install and update the hooks in a new or existing GIT repository:
```bash
git config --global alias.update-hooks '!cd ~/.git_template && git pull > /dev/null && cd - > /dev/null && git init && cp ~/.git_template/hooks/* .git/hooks/ && chmod +x .git/hooks/*'
```

To install the GIT hooks in existing repositories, simply call `git update-hooks` from within each existing repository. Don't worry, this simply installs the files from the template in your `.git` folder.

## Update
To update existing repositories, do call `git update-hooks` again from within the repository. Don't worry, this simply replaces the files from the template in your `.git` folder.
